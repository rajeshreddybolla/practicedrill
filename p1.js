function getwebDevelopers(data){
//   let result = data.filter(employee => employee.job == "Web Developers III" );

//     return result
let result = [];
for( i = 0 ; i < data.length; i++){
    let txt = data[i].job;
  if(txt.startsWith("Web Developer")){
    result.push(data[i])
  }
  }
  return result
}
let d = '$551';
function convertdata(data){
    for(i = 0; i < data.length; i++){
        data[i].salary = formating(data[i].salary)
    }
  return data
}

function formating(string) {
    let str = string.replace("$",'');
       return Math.round((str)*100) /100;   
   }

function correctedSalary(data) {
    for(i = 0; i < data.length; i++){
      data[i].updated_salary = data[i].salary * 10000 
    }
    return data;
}
 
function sumOfSalaries(arr){
    var result = arr.reduce(function (acc, obj) { return acc + obj.updated_salary; }, 0);
return ("Total : "+ result);  
}

function sumOfSalariesByCountry(data){

    let location = {};
    data.forEach(function (arrayItem){
    if(location.hasOwnProperty(arrayItem.location)){
        location[arrayItem.location] += arrayItem.updated_salary;
    }else{
       
        location[arrayItem.location] = arrayItem.updated_salary;
    }
});

return location
}

function avg_salaries(data){
    let location = {};
    let totalsalary = {};
    let avg = {};
    data.forEach(function (arrayItem){
    if(totalsalary.hasOwnProperty(arrayItem.location)){
        totalsalary[arrayItem.location] += arrayItem.updated_salary;
    }else{
       
        totalsalary[arrayItem.location] = arrayItem.updated_salary;
    }
    
    if(location.hasOwnProperty(arrayItem.location)){
        location[arrayItem.location] += 1
    }else{
       
        location[arrayItem.location] = 1
    }
});
    for(key in totalsalary){
        avg[key] = totalsalary[key]/location[key];
    }
    

return avg;
} 

module.exports = {
    getwebDevelopers,
    convertdata,
    correctedSalary,
    sumOfSalaries,
    sumOfSalariesByCountry,
    avg_salaries,

};